---
title: "Timeline"
weight: 2
gallery_img_src: "clock.jpg"
---

- 2024-08-09T11:00:13+00:00 - published [Ensuring Sustainable Open-Source Software Ecosystems: A Standard for Transparent Funding-Flows](/posts/a_standard_for_transparent_funding-flows/)
- 2024-04-09T05:50:13+00:00 - published [Digital Security in Software Supply Chains](/posts/digitale_sicherheit_in_den_software_lieferketten/)
- 2024-01-19T15:00:00+00:00 - published [Concept Paper: Blockchain-Based Flow of Funds for a Sustainable Software Supply Chain](/posts/Blockchain-Based_Flow_of_Funds_for_a_Sustainable_Software_Supply_Chain)
- Mon Oct  9 13:02:00 UTC 2023 - published [Enhancing Security in Software Supply Chains: A Sustainable Approach](/posts/enhancing_security_in_software_supply_chains_a_sustainable_approach)
- Sun Feb 12 10:00:00 UTC 2023 - v0.1.0 release
